# scripts

Infrastructure to setup proxy applications to verify using VTK-m is working
properly.


## setup.sh ##
A bash script that will build VTK-m, vtkh_proxy, and ascent_proxy.
Supports an optional arguments that are the path to the cmnake, abd nvcc compiler
you wish to build everything with.

## azure-pipelines.yml ##
Azure Pipelines script that verifies setup.sh work correctly on hardware that doesn't
have a GPU
