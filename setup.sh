#!/bin/bash
path=$(pwd)

cmake_cmd=cmake
if [ "$1" != "" ]; then
  echo "Using the cmake provided at $1"
  cmake_cmd="$1"
else
  echo "Defaulting to cmake found on path"
fi

if [ "$2" != "" ]; then
  echo "Setting the CUDACXX env variable to the nvcc at $2"
  export CUDACXX="$2"
else
  echo "Defaulting to nvcc found on path"
fi

source_dir="${path}/sources"
build_dir="${path}/builds"
install_dir="${path}/install"

#Note this requires CMake 3.13+ as we are using `-S` and `-B`

#download the require repos
if [ ! -f "${source_dir}/vtkm/CMakeLists.txt" ]; then
  echo "cloning vtkm to ${source_dir}/vtkm"
  git clone --depth=1 https://gitlab.kitware.com/vtk/vtk-m.git "${source_dir}/vtkm"
fi
if [ ! -f "${source_dir}/vtkh_proxy/CMakeLists.txt" ]; then
  echo "cloning vtkh_proxy to ${source_dir}/vtkh_proxy"
  git clone --depth=1 https://gitlab.kitware.com/vtkm_consumer_proxies/vtkh_proxy.git "${source_dir}/vtkh_proxy"
fi
if [ ! -f "${source_dir}/ascent_proxy/CMakeLists.txt" ]; then
  echo "cloning ascent_proxy to ${source_dir}/ascent_proxy"
  git clone --depth=1 https://gitlab.kitware.com/vtkm_consumer_proxies/ascent_proxy.git "${source_dir}/ascent_proxy"
fi

#
# Configure && Build VTK-m
# We cache the build directory so we run this each time

echo "Configuring vtk-m with"
echo "${cmake_cmd} -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=MinSizeRel -DVTKm_ENABLE_CUDA=ON -DVTKm_CUDA_Architecture="volta" VTKm_ENABLE_RENDERING=ON -DVTKm_ENABLE_TESTING=OFF -DVTKm_ENABLE_LOGGING=ON -DVTKm_USE_64BIT_IDS=OFF -DVTKm_USE_DOUBLE_PRECISION=ON"
mkdir -p "${build_dir}/vtkm"
cd "${build_dir}/vtkm"
${cmake_cmd} -DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=MinSizeRel -DVTKm_ENABLE_CUDA=ON -DVTKm_CUDA_Architecture="volta" -DVTKm_ENABLE_RENDERING=ON -DVTKm_ENABLE_TESTING=OFF -DVTKm_ENABLE_LOGGING=ON -DVTKm_USE_64BIT_IDS=OFF -DVTKm_USE_DOUBLE_PRECISION=ON  "${source_dir}/vtkm"
echo "Building vtk-m"
${cmake_cmd} --build "${build_dir}/vtkm" -j 3
${cmake_cmd} --install "${build_dir}/vtkm" --prefix "${install_dir}"


cd ${path}
#
# Configure && Build vtkh_proxy
if [ ! -f "${build_dir}/vtkh_proxy/VTKhProxyTargets.cmake" ]; then
  echo "Configuring vtkh_proxy"
  mkdir -p "${build_dir}/vtkh_proxy"
  cd "${build_dir}/vtkh_proxy"
  ${cmake_cmd} -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="${install_dir}" "${source_dir}/vtkh_proxy"
  cd ${path}
fi
if [ ! -f "${build_dir}/vtkh_proxy/libVTKhProxy.a" ]; then
  echo "Building vtkh_proxy"
  ${cmake_cmd} --build "${build_dir}/vtkh_proxy" -j 3
fi


#
# Configure && Build vtkh_proxy
if [ ! -f "${build_dir}/ascent_proxy/CMakeCache.txt" ]; then
  echo "Configuring ascent_proxy"
  mkdir -p "${build_dir}/ascent_proxy"
  cd "${build_dir}/ascent_proxy"
  ${cmake_cmd} -DCMAKE_BUILD_TYPE=Release -DVTKhProxy_DIR="${build_dir}/vtkh_proxy/" "${source_dir}/ascent_proxy"
  cd ${path}
fi
if [ ! -f "${build_dir}/ascent_proxy/libVTKhProxy.a" ]; then
  echo "Building ascent_proxy"
  ${cmake_cmd} --build "${build_dir}/ascent_proxy" -j 3
fi
